<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */
$router->group(['prefix' => 'api/v1', 'namespace' => 'Api'], function () use ($router) {
    $router->get('/', 'HomeController');

    //
    // Open routes
    //

    // Register new user
    $router->post('signup','SignupUserController');


    // Groups Actions
    $router->get('groups', 'GetGroupsController');
    $router->get('groups/{id:\d+}/users', 'GetGroupsUsersController'); // only accepts integers

    //
    // Authenticated routes
    //
    $router->group(['middleware' => 'auth'], function () use ($router) {

        // user related
        $router->get('me', 'MeController');
        $router->get('me/connections', 'MeConnectionsController');

        // Join to a group
        $router->post('groups/{id:\d+}/join', 'GroupsJoinController'); // only accepts integers

        // connect to user
        $router->post('users/{id:\d+}/connect', 'UsersConnectController'); // only accepts integers


    });

});
