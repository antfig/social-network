
# Introduction
Welcome to Social Network  API!

# Authentication
For all authenticated routes you need to send the `Authorization` header
```
Authorization: Bearer <token>
```

# Response Codes
The API use conventional HTTP response codes to indicate the success or failure of the API request.
Check the table below for reference, you can check the [HTTP Status Codes](https://httpstatuses.com/) for more details about the status.

Code | Description
---------- | -------
200 | Everything is ok.
201 | Resource Created  
400 | Bad Request -- Your request is invalid.
401 | Unauthorized.
403 | Forbidden -- The server understood the request but refuses to authorize it.
404 | Page our Resource Not Found
405 | Method Not Allowed
422 | The payload has missing required parameters or invalid data was given.
429 | Too Many Requests 
500 | Internal Server Error 
503 | Service Unavailable -- We're temporarily offline for maintenance. Please try again later.

## Error samples

### Status: 404

```json
{
    "message": "Page not found.",
    "status_code": 404
}
```
```json
{
    "message": "Resource not found.",
    "status_code": 404
}
```

### Status: 422
```json
{
    "message": "The given data was invalid.",
    "errors": {
        "email": [
            "The email has already been taken."
        ]
    },
    "status_code": 422
}
```

### Status: 403
```json
{
    "message": "Unauthorized",
    "status_code": 403
}
```

    
### Status: 500
```json
{
    "message": "Server Error",
    "status_code": 500
}
```

## Sort And Filter query parameters

### Query Parameters
Parameter | Description
--------- | -----------
sort   | The sort order is ascending unless it is prefixed with a minus (U+002D HYPHEN-MINUS, “-“), in which case is descending. https://jsonapi.org/format/#fetching-sorting
filter | Array of parameters code to search, example: filter[status]=accepted


# Endpoints
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/b75f8ce3a91ffa7ba378#?env%5BSocialNetwork%20-%20local%5D=W3sia2V5IjoidXJsIiwidmFsdWUiOiJodHRwOi8vbG9jYWxob3N0OjgxL2FwaS92MSIsImRlc2NyaXB0aW9uIjoiIiwiZW5hYmxlZCI6dHJ1ZX0seyJ2YWx1ZSI6Im1WSHlRdDV4U2tkbUFlVDROY29Tb1BwbHJtUUh2OVpFcTY5SE9HNjhVUFFOR1ZLR2xVbGRFVE1GRmNXQSIsImtleSI6ImFwaV90b2tlbiIsImVuYWJsZWQiOnRydWV9XQ==)

Use postman to get the list of endpoints or use the postman automatic documentation page
https://documenter.getpostman.com/view/1554509/S1EL31zi

