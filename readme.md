# Social Network Rest API
[![pipeline status](https://gitlab.com/antfig/social-network/badges/master/pipeline.svg)](https://gitlab.com/antfig/social-network/commits/master)
[![coverage report](https://gitlab.com/antfig/social-network/badges/master/coverage.svg)](https://gitlab.com/antfig/social-network/commits/master)

This repo contains the source code for one simple social network api

## API Documentation

Check the [API Documentation](docs/Api_documentaion.md) page for details about how to use the api

**OR**
Import directly the postman collection

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/b75f8ce3a91ffa7ba378#?env%5BSocialNetwork%20-%20local%5D=W3sia2V5IjoidXJsIiwidmFsdWUiOiJodHRwOi8vbG9jYWxob3N0OjgxL2FwaS92MSIsImRlc2NyaXB0aW9uIjoiIiwiZW5hYmxlZCI6dHJ1ZX0seyJ2YWx1ZSI6Im1WSHlRdDV4U2tkbUFlVDROY29Tb1BwbHJtUUh2OVpFcTY5SE9HNjhVUFFOR1ZLR2xVbGRFVE1GRmNXQSIsImtleSI6ImFwaV90b2tlbiIsImVuYWJsZWQiOnRydWV9XQ==)

You can use the collection in `docs/postman` directory or click in the button above.

You can use the postman automatic documentation page as well: 
https://documenter.getpostman.com/view/1554509/S1EL31zi

## Dependencies
- Docker
- Docker-compose

## Install
- Clone this repo to your local environment
- Build docker images
```
docker-compose build
```

- Install composer dependencies
```
docker-compose run --rm php composer install
```

- Copy environment file to your local environment and you can change for your required needs (is configured for docker environment)

```
cp .env.example  .env
```

- Create database schema
```
docker-compose run --rm php php artisan migrate
```

- Now you can start all the containers :)
```
docker-compose up
```
You should be able to access http://localhost:81/api/v1 and see the text "Social Network V1"


- Optionaly you can add dummy data to the database
```
docker-compose run --rm php php artisan db:seed
```


## Usage
- URL: http://localhost:81/api/v1
- Mysql external port: `33060`

Access docker image
```
docker-compose exec php /bin/bash
```


## Testing

```
docker-compose run --rm php vendor/bin/phpunit
docker-compose run --rm php vendor/bin/phpunit --testdox

# With coverage
docker-compose run --rm php vendor/bin/phpunit --coverage-html build
```

## Proposed Production Architecture in AWS
![](docs/architecture.png)

## Objects relations
![](docs/object_relations.png)

## Bussiness logic
- Groups have multiple users
- Users can be in multiple groups
- Users can be connected to multiple users

## TODO
- [ ] Add more tests for api edges cases like wrong filters
- [ ] Add more documentation with swagger
- [ ] Caching results
- [ ] Add endpoint Accept relations
