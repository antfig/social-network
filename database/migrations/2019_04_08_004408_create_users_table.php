<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email', 255);
            $table->string('name', 255)
                ->nullable();
            $table->string('password', 255);
            $table->string('api_token', 255)
                ->unique()
                ->nullable()
                ->default(null);

            $table->timestamps();
            $table->softDeletes();

            $table->index(['name', 'created_at']); // used in sorting

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
