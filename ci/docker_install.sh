#!/bin/bash
# Based in gitlab documentation https://docs.gitlab.com/ee/ci/examples/php.html

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git and zip (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git zip -yqq

# Install composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# install php extensions
docker-php-ext-install pdo_mysql

# install xdebug for code coverage
pecl install xdebug
docker-php-ext-enable xdebug
