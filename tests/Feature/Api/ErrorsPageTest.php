<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use Tests\Feature\BaseTestCase;

class ErrorsPageTest extends BaseTestCase
{

    /** @var string */
    protected $url = 'api/v1';

    public function testCanSee404ErrorForNotFoundPages()
    {
        $url = $this->url . '/not_found_page';

        $expectedError = [
            'message'     => 'Page not found.',
            'status_code' => 404,
        ];

        $response = $this->json('GET', $url);

        $response->seeStatusCode(404)
            ->seeJsonContains($expectedError);
    }

}
