<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\Feature\BaseTestCase;

class SignupUserTest extends BaseTestCase
{
    use DatabaseTransactions;

    /** @var string */
    protected $signupUrl = 'api/v1/signup';

    public function testCanSignupNewUser()
    {
        $user = $this->getNewUser();

        $response = $this->post($this->signupUrl, [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
        ]);

        $response->seeStatusCode(201)
            ->seeJsonStructure([
                    'data' => [
                        'id',
                        'name',
                        'email',
                        'api_token',
                        'created_at',
                        'updated_at'
                    ]
                ]
            )->seeJsonContains([
                'name' => $user->name,
                'email' => $user->email,
            ]);
    }

    public function testUserEmailIsUnique()
    {

        $user = $this->getNewUser();


        $response = $this->post($this->signupUrl, [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
        ]);

        // should create without errors
        $response->seeStatusCode(201);

        // when try to register the user with the same email
        $response = $this->post($this->signupUrl, [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
        ]);

        // should see status 422 and error message
        $response->seeStatusCode(422)
            ->seeJsonStructure([
                'message',
                'errors' => [
                    'email',
                ],
                'status_code',
            ])->seeJson([
                'errors' => [
                    'email' => [trans('validation.unique', ['attribute' => 'email'])]
                ]
            ]);
    }

    public function testCanNotSignupWithoutRequiredFields()
    {
        $response = $this->post($this->signupUrl, []);

        $response->seeStatusCode(422)
            ->seeJsonStructure([
                'message',
                'errors' => [
                    'name',
                    'email',
                    'password',
                ],
                'status_code',
            ])->seeJson([
                'errors' => [
                    'name' => [trans('validation.required', ['attribute' => 'name'])],
                    'email' => [trans('validation.required', ['attribute' => 'email'])],
                    'password' => [trans('validation.required', ['attribute' => 'password'])],
                ]
            ]);
    }

    public function testCanNotSignupWithInvalidFields()
    {
        $response = $this->post($this->signupUrl, [
            'name' => 'Jon dow',
            'email' => 'invalid_email',
            'password' => 'a',
        ]);

        $response->seeStatusCode(422)
            ->seeJsonStructure([
                'message',
                'errors' => [
                    'email',
                    'password',
                ],
                'status_code',
            ])->seeJson([
                'errors' => [
                    'email' => [trans('validation.email', ['attribute' => 'email'])],
                    'password' => [trans('validation.min.string', ['attribute' => 'password', 'min' => 6])]
                ]
            ]);
    }

}
