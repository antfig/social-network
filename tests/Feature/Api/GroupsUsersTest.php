<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use App\Models\Group;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\Feature\BaseTestCase;

class GroupsUsersTest extends BaseTestCase
{
    use DatabaseTransactions;

    /** @var string */
    protected $url = 'api/v1/groups/';

    public function testCanGetGroupUsers()
    {
        // add some data
        /** @var Group $group */
        $group = factory(Group::class)->create();

        $group->users()->save($this->getNewUser());
        $group->users()->save($this->getNewUser());

        $url = $this->url . $group->id . '/users';

        $structure = [
            'data' => [
                [
                    'id',
                    'name',
                    'joined_at',
                ]
            ]
        ];

        $response = $this->get($url);

        $response->seeStatusCode(200)
            ->seeJsonStructure($this->withPaginationStructure($structure));
    }

    public function testCanNotGetUsersForNonexistentGroup()
    {
        $url = $this->url .'999999999999' . '/users';

        $response = $this->get($url);

        $response->seeStatusCode(404)
            ->seeJson([
                'message' => 'Resource not found.',
                'status_code' => 404
            ]);
    }
}
