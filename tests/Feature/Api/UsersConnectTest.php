<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\Feature\BaseTestCase;

class UsersConnectTest extends BaseTestCase
{
    use DatabaseTransactions;

    /** @var string */
    protected $url = 'api/v1/users/';

    /** @var User */
    protected $fromUser;

    /** @var User */
    protected $userToConnect;

    protected function setUp(): void
    {
        parent::setUp();

        $this->fromUser = $this->getNewUser();
        $this->fromUser->save();

        $this->userToConnect = $this->getNewUser();
        $this->userToConnect->save();
    }

    public function testCanConnectWithSomeUser()
    {
        $url = $this->url . $this->userToConnect->id . '/connect';

        $response = $this->post($url, [], [
            'Authorization' => 'Bearer ' . $this->fromUser->api_token
        ]);

        $response->seeStatusCode(200)
            ->seeJson([
                'status' => 'ok'
            ]);
    }

    public function testUserCanNotConnectWithHimSelf()
    {
        $url = $this->url . $this->fromUser->id . '/connect';

        // can join the first time
        $response = $this->post($url, [], [
            'Authorization' => 'Bearer ' . $this->fromUser->api_token
        ]);

        $response->seeStatusCode(422)
            ->seeJson([
                'message' => 'You can not connect with yourself',
                'status_code' => 422
            ]);
    }

    public function testUserCanNotConnectWithAlreadyConnectedUser()
    {
        $url = $this->url . $this->userToConnect->id . '/connect';

        // can join the first time
        $response = $this->post($url, [], [
            'Authorization' => 'Bearer ' . $this->fromUser->api_token
        ]);

        $response->seeStatusCode(200);

        // when join again receive validation errors
        $response = $this->post($url, [], [
            'Authorization' => 'Bearer ' . $this->fromUser->api_token
        ]);

        $response->seeStatusCode(422)
            ->seeJson([
                'message' => 'You are already connected with this user',
                'status_code' => 422
            ]);

    }
}
