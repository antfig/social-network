<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\Feature\BaseTestCase;

class MeConnectionsTest extends BaseTestCase
{
    use DatabaseTransactions;

    /** @var string */
    protected $url = 'api/v1/me/connections';

    /** @var User */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();
        
        /** @var User $user */
        $this->user = $this->getNewUser();
        $this->user->save();

        // add 5 connections to user;
        $users = factory(User::class, 5)->create();
        $this->user->connections()->saveMany($users);
    }

    public function testCanGetUserConnections()
    {
        $response = $this->get($this->url, [
            'Authorization' => 'Bearer ' . $this->user->api_token
        ]);

        $structure = [
            'data' => [
                [
                    'user_id',
                    'name',
                    'created_at',
                    'status',
                ]
            ]
        ];

        $response->seeStatusCode(200)
            ->seeJsonStructure($this->withPaginationStructure($structure));
    }

    public function testCanSortConnections()
    {
        $url = $this->url . '?sort=-name';

        $response = $this->get($url, [
            'Authorization' => 'Bearer ' . $this->user->api_token
        ]);

        $response->seeStatusCode(200);
        // missing the validation of the sort
    }

    public function testCanFilterConnections()
    {
        $url = $this->url . '?filter[status]=accepted&filter[name]=ant';

        $response = $this->get($url, [
            'Authorization' => 'Bearer ' . $this->user->api_token
        ]);

        $response->seeStatusCode(200);
        // missing the validation of the filters
    }
}
