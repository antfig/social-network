<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use App\Models\Group;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\Feature\BaseTestCase;

class GroupsJoinTest extends BaseTestCase
{
    use DatabaseTransactions;

    /** @var string */
    protected $url = 'api/v1/groups/';

    public function testUserCanJoinToGroup()
    {
        $user = $this->getNewUser();
        $user->save();
        $group = factory(Group::class)->create();
        $url = $this->url . $group->id . '/join';

        $response = $this->post($url, [], [
            'Authorization' => 'Bearer ' . $user->api_token
        ]);

        $response->seeStatusCode(200)
            ->seeJson([
                'status' => 'ok'
            ]);
    }

    public function testUserCanNotJoinWithInvalidToken()
    {
        $group = factory(Group::class)->create();

        $url = $this->url . $group->id . '/join';

        $response = $this->post($url, [], [
            'Authorization' => 'Bearer INVALID_TOKEN'
        ]);

        $response->seeStatusCode(403)
            ->seeJson([
                'message' => 'Unauthorized',
                'status_code' => 403
            ]);
    }

    public function testUserCanNotJoinTwiceToTheSameGroup()
    {
        $user = $this->getNewUser();
        $user->save();

        $group = factory(Group::class)->create();

        $url = $this->url . $group->id . '/join';

        // can join the first time
        $response = $this->post($url, [], [
            'Authorization' => 'Bearer ' . $user->api_token
        ]);

        $response->seeStatusCode(200);

        // when join again receive validation errors
        $response = $this->post($url, [], [
            'Authorization' => 'Bearer ' . $user->api_token
        ]);

        $response->seeStatusCode(422)
            ->seeJson([
                'message' => 'User already belongs to the group',
                'status_code' => 422
            ]);
    }
}
