<?php
declare(strict_types=1);


namespace Tests\Feature\Api;

use Tests\Feature\BaseTestCase;

class HomeTest extends BaseTestCase
{
    /** @var string */
    protected $url = 'api/v1';

    public function testCanAccessBaseUrlOfApi()
    {
        $this->get($this->url);

        $this->seeStatusCode(200)
            ->seeJson(['Social Network V1']);
    }

}
