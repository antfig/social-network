<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use App\Models\Group;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\Feature\BaseTestCase;

class MeTest extends BaseTestCase
{
    use DatabaseTransactions;

    /** @var string */
    protected $url = 'api/v1/me';

    public function testUserDetailsByToken()
    {
        $user = $this->getNewUser();
        $user->save();

        $group = factory(Group::class)->create();
        $user->groups()->save($group); // add the group to the user to get in the api

        // can join the first time
        $response = $this->get($this->url, [
            'Authorization' => 'Bearer ' . $user->api_token
        ]);

        $response->seeStatusCode(200)
            ->seeJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'email',
                    'api_token',
                    'created_at',
                    'updated_at',
                    'groups' => [
                        [
                            'id',
                            'name',
                            'joined_at'
                        ]
                    ]
                ]
            ])
            // to ensure correct data
            ->seeJson([
                'id' => $user->id,
                'name' => $user->name
            ]);
    }

}
