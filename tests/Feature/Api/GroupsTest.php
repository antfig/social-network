<?php
declare(strict_types=1);

namespace Tests\Feature\Api;

use App\Models\Group;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\Feature\BaseTestCase;

class GroupsTest extends BaseTestCase
{
    use DatabaseTransactions;

    /** @var string */
    protected $url = 'api/v1/groups';

    public function testCanGetGroupsPaginated()
    {
        // add some data
        factory(Group::class, 5)->create();

        $response = $this->get($this->url);

        $structure = [
            'data' => [
                [
                    'id',
                    'name',
                    'created_at',
                ]
            ]
        ];

        $response->seeStatusCode(200)
            ->seeJsonStructure($this->withPaginationStructure($structure));
    }


}
