<?php
declare(strict_types=1);

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

class BaseTestCase extends TestCase
{
    protected $paginationStructure = [
        'links' => [
          'first',
          'last',
          'prev',
          'next'
        ],
        'meta' => [
            'current_page',
            'from',
            'last_page',
            'path',
            'per_page',
            'to',
            'total',
        ]
    ];

    /**
     * @return array
     */
    protected function getPaginationStructure() : array
    {
        return $this->paginationStructure;
    }

    /**
     * @param array $structure
     * @return array
     */
    protected function withPaginationStructure(array $structure): array
    {
        return array_merge($structure, $this->getPaginationStructure());
    }

    /**
     * Get new instance off User without storing in database
     *
     * @return User
     */
    protected function getNewUser(): User
    {
        return factory(User::class)->make([
            'api_token' => Str::random(60)
        ]);
    }

}
