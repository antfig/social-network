<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     *
     * @return void
     * @throws Exception
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $e
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $e)
    {
        // validations errors
        if ($e instanceof ValidationException && $e->getResponse()) {
            return $this->invalidJson($e);
        }

        $e = $this->prepareException($e);

        return $this->prepareJsonResponse($e);
    }

    /**
     * Convert a validation exception into a JSON response.
     *
     * @param  \Illuminate\Validation\ValidationException $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function invalidJson(ValidationException $exception)
    {
        return new JsonResponse([
            'message'     => $exception->getMessage(),
            'errors'      => $exception->errors(),
            'status_code' => $exception->status
        ], $exception->status);
    }

    /**
     * Prepare a JSON response for the given exception.
     *
     * @param  \Exception $e
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function prepareJsonResponse(Exception $e)
    {
        $status  = $this->isHttpException($e) ? $e->getStatusCode() : 500;
        $headers = $this->isHttpException($e) ? $e->getHeaders() : [];

        return new JsonResponse($this->convertExceptionToArray($e, $status), $status, $headers);
    }

    /**
     * Convert the given exception to an array.
     * Using the format: [
     *  'message => 'Error description',
     *  'status_code' => 'http_status code'
     *  'debug' => 'when in debug mode'
     * ]
     *
     * @param  \Exception $e
     *
     * @param int         $statusCode
     *
     * @return array
     */
    protected function convertExceptionToArray(Exception $e, int $statusCode)
    {
        return $this->isDebugMode() ? [
            'message'     => $e->getMessage(),
            'status_code' => $statusCode,
            'debug'       => [
                'exception' => get_class($e),
                'file'      => $e->getFile(),
                'line'      => $e->getLine(),
                'trace'     => collect($e->getTrace())->map(function ($trace) {
                    return Arr::except($trace, ['args']);
                })->all()
            ],
        ] : [
            'message'     => $this->isHttpException($e) ? $e->getMessage() : 'Server Error',
            'status_code' => $statusCode,
        ];
    }

    /**
     * Determine if the given exception is an HTTP exception.
     *
     * @param  \Exception $e
     *
     * @return bool
     */
    protected function isHttpException(Exception $e)
    {
        return $e instanceof HttpException;
    }

    /**
     * @param Exception $e
     *
     * @return Exception
     */
    protected function prepareException(Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException('Resource not found.', $e);
        } elseif ($e instanceof AuthorizationException) {
            $e = new HttpException(403, $e->getMessage());
        } elseif ($e instanceof NotFoundHttpException && !$e->getMessage()) {
            $e = new NotFoundHttpException('Page not found.');
        }

        return $e;
    }

    protected function isDebugMode()
    {
        return env('APP_DEBUG', config('app.debug', false));
    }
}
