<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     */
    public function boot()
    {
        // Use only Authorization to receive the user token

        $this->app['auth']->viaRequest('api', function (Request $request) {

            if (null !== $request->bearerToken()) {
                return User::where('api_token', $request->bearerToken())->first();
            }
        });
    }
}
