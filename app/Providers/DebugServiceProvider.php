<?php
declare(strict_types=1);

namespace App\Providers;

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;

/**
 * @codeCoverageIgnore
 */
class DebugServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot(DatabaseManager $databaseManager, LoggerInterface $logger)
    {
        $databaseManager->enableQueryLog();

        $logger->info('----------------- New Query Logger -------------');
        // log all the queries to log file
        $databaseManager->listen(function (QueryExecuted $query) use ($logger) {
            $logger->debug($query->sql, [$query->bindings, $query->time . "ms"]);
        });
    }
}

