<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Factories\UserFactory;
use Illuminate\Http\Request;

class SignupUserController extends Controller
{

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function __invoke(Request $request)
    {
        $this->ensureValidData($request);

        $newUser = (new UserFactory())->make($request->only('email', 'name', 'password'));

        return new UserResource($newUser);
    }

    /**
     * Validate form request
     *
     * @param  array $data
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function ensureValidData(Request $request)
    {
        $rules = [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'min:6'],
        ];

        $this->validate($request, $rules);
    }
}
