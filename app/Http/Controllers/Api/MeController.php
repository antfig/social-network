<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class MeController extends Controller
{
    public function __invoke()
    {
        /** @var User $user */
        $user = Auth::user();
        $user->load('groups'); // get user connected groups

        return new UserResource($user);
    }
}
