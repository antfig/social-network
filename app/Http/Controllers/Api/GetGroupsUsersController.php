<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserGroupResource;
use App\Models\Group;

class GetGroupsUsersController extends Controller
{
    /**
     * @param int $id
     */
    function __invoke(int $id)
    {
        /** @var Group $group */
        $group = Group::findOrFail($id);
        $users = $group->users()->paginate(self::DEFAULT_PAGINATION_PER_PAGE);

        return UserGroupResource::collection($users);
    }
}
