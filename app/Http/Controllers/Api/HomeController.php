<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    function __invoke()
    {
        return response()->json('Social Network V1');
    }
}
