<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Filters;
use App\Http\Resources\UserConnectionResource;
use App\Http\Sort;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MeConnectionsController extends Controller
{

    /**
     * Available sorting options with related key for sort
     * @var array
     */
    protected $sortingOptions = [
        'created_at' => 'pivot_created_at',
        'name'       => 'users.name'
    ];

    /**
     * Available filters options
     * @var array
     */
    protected $filtersOptions = [
        'status' => 'user_connections.status',
        'name'   => 'users.name'
    ];

    public function __invoke(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $sort = new Sort($request, $this->sortingOptions);
        $filters = new Filters($request, $this->filtersOptions);

        $connections = $user->connections();

        if ($sort->hasSort()) {
            $connections = $connections->orderBy($sort->getColumn(), $sort->direction());
        }

        if ($filters->hasFilters()) {
            $connections = $connections->where($filters->get());
        }

        $connections = $connections->paginate(self::DEFAULT_PAGINATION_PER_PAGE)
                        ->appends($request->all());

        return UserConnectionResource::collection($connections);
    }

}
