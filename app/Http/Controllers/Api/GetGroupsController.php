<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\GroupResource;
use App\Models\Group;

class GetGroupsController extends Controller
{
    public function __invoke()
    {
        return GroupResource::collection(Group::paginate(self::DEFAULT_PAGINATION_PER_PAGE));
    }
}
