<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class GroupsJoinController extends Controller
{
    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(int $id)
    {
        /** @var Group $group */
        $group = Group::findOrFail($id);

        /** @var User $user */
        $user = Auth::user();

        $this->ensureNotAlreadyJoin($user, $group);

        $user->groups()->save($group);

        return response()->json([
            'status' => 'ok'
        ]);

    }

    /**
     * @param User $user
     * @param Group $group
     */
    private function ensureNotAlreadyJoin(User $user, Group $group): void
    {
        $alreadyJoin = $user->groups()->wherePivot('group_id', $group->id)->count();

        if ($alreadyJoin > 0) {
            throw new HttpException(422, 'User already belongs to the group');
        }
    }

}
