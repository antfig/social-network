<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ValueObjects\Status;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UsersConnectController extends Controller
{
    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(int $id)
    {
        /** @var User $user */
        $user = Auth::user();

        $this->ensureNotSameUser($user, $id);

        $userToConnect = User::findOrFail($id);

        $this->ensureNotAlreadyConnected($user, $userToConnect);

        $user->connections()->save($userToConnect, ['status' => Status::pending()]);

        return response()->json([
            'status' => 'ok'
        ]);
    }

    /**
     * @param User $fromUser
     * @param User $toUser
     */
    private function ensureNotAlreadyConnected(User $fromUser, User $toUser)
    {
        $alreadyJoin = $fromUser->connections()->wherePivot('to_user_id', $toUser->id)->count();

        if ($alreadyJoin > 0) {
            throw new HttpException(422, 'You are already connected with this user');
        }
    }

    /**
     * @param User $user
     * @param int $id
     */
    private function ensureNotSameUser(User $user, int $id)
    {
        if ($user->id === $id) {
            throw new HttpException(422, 'You can not connect with yourself');

        }
    }
}
