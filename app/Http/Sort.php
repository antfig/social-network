<?php
declare(strict_types=1);

namespace App\Http;

use Illuminate\Http\Request;

final class Sort
{
    const SORT_KEY = 'sort';

    /**
     * @var array
     */
    private $sortMapping;

    /**
     * @var bool
     */
    private $hasSort = false;

    /**
     * @var string|null
     */
    private $sortValue;

    /**
     * Sort direction
     * @var string|null
     */
    private $direction;

    /**
     * Sort column
     * @var string|null
     */
    private $column;

    /**
     * @param Request $request
     * @param array $sortMapping
     */
    public function __construct(Request $request, array $sortMapping)
    {
        $this->sortValue = $request->get(self::SORT_KEY);
        $this->sortMapping = $sortMapping;

        $this->initSorting();
    }

    /**
     * @return bool
     */
    public function hasSort(): bool
    {
        return $this->hasSort;
    }

    /**
     * @return string|null
     */
    public function getColumn(): ?string
    {
        return $this->column;
    }

    /**
     * @return string|null
     */
    public function direction(): ?string
    {
        return $this->direction;
    }

    private function initSorting(): void
    {
        // empty sort return empty array
        if (null === $this->sortValue) {
            return;
        }

        // remove the sorting direction
        $property = ltrim($this->sortValue, '-');

        // ensure sorting options
        if (!array_key_exists($property, $this->sortMapping)) {
            return;
        }

        $this->direction = $this->sortValue[0] === '-' ? 'desc' : 'asc';
        $this->column = $this->sortMapping[$property];
        $this->hasSort = true;
    }
}
