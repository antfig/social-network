<?php
declare(strict_types=1);

namespace App\Http;

use Illuminate\Http\Request;

final class Filters
{
    const FILTERS_KEY = 'filter';

    /**
     * @var array
     */
    private $filtersMapping;

    /**
     * @var array|null
     */
    private $filtersRequest;

    /**
     * @var array|null
     */
    private $filters = [];

    public function __construct(Request $request, array $filtersMapping)
    {
        $this->filtersRequest = $request->get(self::FILTERS_KEY);
        $this->filtersMapping = $filtersMapping;

        $this->initFilters();
    }

    /**
     * @return bool
     */
    public function hasFilters(): bool
    {
        return !empty($this->filters);
    }

    /**
     * @return array
     */
    public function get(): array
    {
        return $this->filters;
    }

    private function initFilters(): void
    {
        // only accept arrays
        if (!is_array($this->filtersRequest)) {
            return;
        }

        foreach ($this->filtersRequest as $filter => $value) {

            if (!array_key_exists($filter, $this->filtersMapping)
                || empty($value)) {
                continue;
            }

            $this->filters[] = [
                $this->filtersMapping[$filter],
                'like',
                '%' . strtolower($value) . '%'
            ];
        }
    }

}
