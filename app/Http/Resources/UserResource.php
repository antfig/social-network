<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'api_token' => $this->api_token,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            // only include groups if they are loaded to avoid doing multiple queries
            'groups' => UserGroupResource::collection($this->whenLoaded('groups')),
            'connections' => UserConnectionResource::collection($this->whenLoaded('connections'))
        ];
    }
}
