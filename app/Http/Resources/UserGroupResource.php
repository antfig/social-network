<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserGroupResource extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name, //name of the users or of the group
            'joined_at' => $this->whenPivotLoaded('group_users', function () {
                return $this->pivot->created_at;
            }),
        ];
    }
}
