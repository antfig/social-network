<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserConnectionResource extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->id,
            'name' => $this->name,
            'created_at' => $this->whenPivotLoaded('user_connections', function () {
                return $this->pivot->created_at;
            }),
            'status' => $this->whenPivotLoaded('user_connections', function () {
                return $this->pivot->status;
            })
        ];
    }
}
