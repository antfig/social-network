<?php
declare(strict_types=1);

namespace App\Models\ValueObjects;

use InvalidArgumentException;

final class Status
{
    /**
     * @var array
     */
    protected $allowedStatus = [
        'pending',
        'accepted',
        'refused'
    ];

    /**
     * @var string
     * @throw InvalidArgumentException
     */
    private $status;

    public function __construct(string $status)
    {
        $this->ensureStatus($status);

        $this->status = $status;
    }

    /**
     * @return string
     */
    public function __toString()
    {
      return $this->status;
    }

    /**
     * @param string $status
     */
    private function ensureStatus(string $status)
    {
        if (!in_array($status, $this->allowedStatus)) {
            throw new InvalidArgumentException();
        }
    }

    // factories

    /**
     * @return Status
     */
    public static function pending(): self
    {
        return new static('pending');
    }

    /**
     * @return Status
     */
    public static function accepted(): self
    {
        return new static('accepted');
    }

    /**
     * @return Status
     */
    public static function refused(): self
    {
        return new static('refused');
    }
}
