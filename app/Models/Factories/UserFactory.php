<?php
declare(strict_types=1);

namespace App\Models\Factories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory
{
    /**
     * @param array $values
     *
     * @return User
     */
    public function make(array $values): User
    {
        $user = new User();
        $user->email = $values['email'];
        $user->password = Hash::make($values['password']);
        $user->name = $values['name'];
        $user->api_token = Str::random(60);
        $user->save();

        return $user;
    }
}
